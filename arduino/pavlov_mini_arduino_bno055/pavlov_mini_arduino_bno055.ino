#include <ros.h>
#include <Wire.h>

#include <sensor_msgs/JointState.h>
#include <pavlov_mini_msgs/set_motor_calibration.h>
#include <pavlov_mini_msgs/connect_servos.h>
#include <pavlov_mini_msgs/contact_detection.h>

#include <std_msgs/Float64.h>
#include <sensor_msgs/Imu.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>


#include "configuration.h"
#include "actuator.h"
#include <Servo.h>
#include "FeetContactDetector.h"


/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (1)



/*********************************************
************* PINS CONFIGURATION *************
**********************************************/


char *servoNames[] = {"hip1_fl", "hip2_fl", "knee_fl",
                      "hip1_fr", "hip2_fr", "knee_fr",
                      "hip1_bl", "hip2_bl", "knee_bl",
                      "hip1_br", "hip2_br", "knee_br"};



int PIN_SERVOS[NUM_MOTORS]  = {0,1,2,
                               6,7,8,
                               3,4,5,
                               9,10,11};

//int PIN_SERVOS[NUM_MOTORS]  = {2,  1,  0,
//                               8,  7,  6,
//                               5,  4,  3,
//                               11,  10, 9};



int PIN_FEET_CONTACT[4] = {23, 22, 21, 20};



int PIN_SERVO_SWITCH = 16;




/***************** MAIN VARIABLES *****************/
Actuator* servomotors[NUM_MOTORS];
ros::NodeHandle  nh;


                                  
Adafruit_BNO055 imuSensor = Adafruit_BNO055(-1, 0x28);

FeetContactDetector stance_fl(PIN_FEET_CONTACT[0]);
FeetContactDetector stance_fr(PIN_FEET_CONTACT[1]);
FeetContactDetector stance_bl(PIN_FEET_CONTACT[2]);
FeetContactDetector stance_br(PIN_FEET_CONTACT[3]);




/***************** LOGS *****************/
void logInfo(String msg){
    nh.loginfo(msg.c_str());
}

void logError(String msg){
    nh.logerror(msg.c_str());
}



/*********************************************
************* CALLBACKS FUNCTIONS ************
**********************************************/


void setMotorCalCB(const pavlov_mini_msgs::set_motor_calibration::Request & req, pavlov_mini_msgs::set_motor_calibration::Response & res){
  int i = req.servo_index;

  if (!req.setCalibration_servo){
    servomotors[i]->setServoCalibration(false);
  }
  
  else{
    servomotors[i]->setServoCalibration(req.min_calAngle,  req.max_calAngle,
                                        req.min_posServo,  req.max_posServo,
                                        req.min_posAngle,  req.max_posAngle);
                                        
    servomotors[i]->setLimPositions(req.min_lim_angle, req.max_lim_angle);

  }

                                 
  res.result = true;
  logInfo( "Calibration for motor " + servomotors[i]->getName() + " (" + String(i) + "),  Servo cal: " + String( servomotors[i]->isServoCalibrated()));

}



void connectServosCB(const pavlov_mini_msgs::connect_servos::Request & req, pavlov_mini_msgs::connect_servos::Response & res){
  bool connect = req.connect;
  if (connect){
      for(short i=0; i < NUM_MOTORS; i++){
        servomotors[i]->connect();
      }
      res.result = true;
      logInfo("Servomotors attached");
      digitalWrite(PIN_SERVO_SWITCH, LOW);
      
  }
  else{
      for(short i=0; i < NUM_MOTORS; i++){
          servomotors[i]->disconnect();
      }
      res.result = true;
      logInfo("Servomotors disattached");
      digitalWrite(PIN_SERVO_SWITCH, HIGH);


  }


  
}



void jointControlCB(const sensor_msgs::JointState& msg){


  if(msg.position_length != NUM_MOTORS){
    logError("Invalid joint control msg. Either position array or effort array is empty!.");
    return;
  }

  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i]->setPosition(msg.position[i]);
  }
}




/************************ ROS SERVICES ************************/

ros::ServiceServer<pavlov_mini_msgs::set_motor_calibration::Request, pavlov_mini_msgs::set_motor_calibration::Response> setMotorCalServer("pavlov_mini_muscular/set_motor_calibration",&setMotorCalCB);
ros::ServiceServer<pavlov_mini_msgs::connect_servos::Request, pavlov_mini_msgs::connect_servos::Response> connectServosServer("pavlov_mini_muscular/connect_servos",&connectServosCB);


/************************ ROS SUBSCRIBERS ************************/

ros::Subscriber<sensor_msgs::JointState> jointControlSub("pavlov_mini_muscular/joints_control", &jointControlCB );


/************************ ROS PUBLISHERS ************************/
sensor_msgs::Imu imu_state;
std_msgs::Float64 loop_freq;
pavlov_mini_msgs::contact_detection feet_contact_detector;

ros::Publisher IMUStatePub("pavlov_mini_muscular/imu_data",       &imu_state);
ros::Publisher freqInfoPub("pavlov_mini_muscular/driver_freq",    &loop_freq);
ros::Publisher contactDetectionPub("pavlov_mini_muscular/contact_detection",    &feet_contact_detector);



/************************ TIMERS ************************/
unsigned long Timer;
unsigned long TimerPublisher;
unsigned long TimerFreqPubliser;

unsigned int counts_loop;


int imuPublishCounter = 0;
sensors_event_t angVelocityData , linearAccelData;


/***************************************************************
 * ********************** SETUP FUNCTION ***********************
 ***************************************************************/


void setup() {


  nh.initNode();
  nh.advertise(IMUStatePub);
  nh.advertise(freqInfoPub);
  nh.advertise(contactDetectionPub);

  nh.advertiseService(setMotorCalServer);
  nh.advertiseService(connectServosServer);

  nh.subscribe(jointControlSub);



  //Wire.begin();
  //imuSensor.begin();
  //Wire.setClock(200000); //Increase I2C data rate to 400kHz
  
  //imuSensor.enableGyroIntegratedRotationVector(BNO080_SAMPLERATE_DELAY_MS);
  //imuSensor.enableLinearAccelerometer(BNO080_SAMPLERATE_DELAY_MS);  

  if(!imuSensor.begin()){
      /* There was a problem detecting the BNO055 ... check your connections */
    logError("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while(1);
  }
  imuSensor.setExtCrystalUse(true);

  
  Timer             = micros();
  TimerPublisher    = micros();
  TimerFreqPubliser = micros();


  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i] = new Actuator(&nh, i, servoNames[i], PIN_SERVOS[i]);
      //servomotors[i]->connect();
      //servomotors[i]->disconnect();
  }

  pinMode(PIN_SERVO_SWITCH, OUTPUT);


    imu_state.orientation.w = 1;
    imu_state.orientation.x = 0;
    imu_state.orientation.y = 0;
    imu_state.orientation.z = 0;

    imu_state.angular_velocity.x =  0;
    imu_state.angular_velocity.y =  0;
    imu_state.angular_velocity.z =  0;


    imu_state.linear_acceleration.x = 0;
    imu_state.linear_acceleration.y = 0;
    imu_state.linear_acceleration.z = 0;

    feet_contact_detector.feet_stance_length = 4;
  
}




void initImuState(){
    imu_state.orientation.w = 1;
    imu_state.orientation.x = 0;
    imu_state.orientation.y = 0;
    imu_state.orientation.z = 0;

    imu_state.angular_velocity.x =  0;
    imu_state.angular_velocity.y =  0;
    imu_state.angular_velocity.z =  0;


    imu_state.linear_acceleration.x = 0;
    imu_state.linear_acceleration.y = 0;
    imu_state.linear_acceleration.z = 0;
}



void getImuData(){

    if (imuPublishCounter == 0){
      initImuState();
    }



    imu::Quaternion quat = imuSensor.getQuat();
    imuSensor.getEvent(&angVelocityData, Adafruit_BNO055::VECTOR_GYROSCOPE);
    imuSensor.getEvent(&linearAccelData, Adafruit_BNO055::VECTOR_LINEARACCEL);
  
    imu_state.orientation.w = -quat.x();
    imu_state.orientation.x = -quat.z();
    imu_state.orientation.y = quat.w();
    imu_state.orientation.z = -quat.y();


    imuSensor.getEvent(&linearAccelData, Adafruit_BNO055::VECTOR_LINEARACCEL);
    imu_state.angular_velocity.x += linearAccelData.gyro.y;// -imuSensor.getFastGyroY();
    imu_state.angular_velocity.y += linearAccelData.gyro.x;// -imuSensor.getFastGyroX();
    imu_state.angular_velocity.z += -linearAccelData.gyro.z;// -imuSensor.getFastGyroZ();


    imu_state.linear_acceleration.x += linearAccelData.acceleration.y;// -imuSensor.getLinAccelY();
    imu_state.linear_acceleration.y += linearAccelData.acceleration.x;// -imuSensor.getLinAccelX();
    imu_state.linear_acceleration.z += -linearAccelData.acceleration.z;// -imuSensor.getLinAccelZ();

    
    imuPublishCounter += 1;
}














/***************************************************************
 * **************** PUBLISH DATA FUNCTION ***************
 ***************************************************************/

void sendRobotData(){
    if(micros() - TimerPublisher < 1000000/PUBLISH_DATA_FREQ){
      return;
    }

    

    /************ IMU STATE ***********/

    if(imuPublishCounter > 0){
      imu_state.angular_velocity.x = imu_state.angular_velocity.x/imuPublishCounter;
      imu_state.angular_velocity.y = imu_state.angular_velocity.y/imuPublishCounter;
      imu_state.angular_velocity.z = imu_state.angular_velocity.z/imuPublishCounter;
  
  
      imu_state.linear_acceleration.x = imu_state.linear_acceleration.x/imuPublishCounter;
      imu_state.linear_acceleration.y = imu_state.linear_acceleration.y/imuPublishCounter;
      imu_state.linear_acceleration.z = imu_state.linear_acceleration.z/imuPublishCounter;
    }

    
    imu_state.header.stamp = nh.now();
    imu_state.header.frame_id = "base_link";
    IMUStatePub.publish( &imu_state );
    

    imuPublishCounter = 0;


    imu_state.angular_velocity.x =  0;
    imu_state.angular_velocity.y =  0;
    imu_state.angular_velocity.z =  0;


    imu_state.linear_acceleration.x = 0;
    imu_state.linear_acceleration.y = 0;
    imu_state.linear_acceleration.z = 0;

    TimerPublisher = micros();    

    feet_contact_detector.header.stamp = nh.now();

    //feet_contact_detector.feet_stance[0] = 0;
    //feet_contact_detector.feet_stance[1] = 0;
    //feet_contact_detector.feet_stance[2] = 0;
    //feet_contact_detector.feet_stance[3] = 0;

    bool stance[4] = {stance_fl.getStance(), stance_fr.getStance(), stance_bl.getStance(), stance_br.getStance()};

    //int val[4] = {stance_fl.get_analog_val(), stance_fr.get_analog_val(), stance_bl.get_analog_val(), stance_br.get_analog_val()};
    //logInfo(String(val[0]));
    
    feet_contact_detector.feet_stance = stance;
    contactDetectionPub.publish(&feet_contact_detector);
}








/***************************************************************
 * **************** PUBLISH FREQ FUNCTION **********************
 ***************************************************************/

 
void publishInfoFreq(){
    unsigned long deltaT = micros()  - TimerFreqPubliser;
    if(deltaT < 1000000/PUBLISH_INFO_FREQ){
      return;
    }

    double freq = double(deltaT)/double(counts_loop);
    freq = 1000000/freq;

    loop_freq.data = freq;
    freqInfoPub.publish( &loop_freq );


    TimerFreqPubliser = micros();
    counts_loop = 0;
}








/***************************************************************
 * **************** LOOP FUNCTION *****************************
 ***************************************************************/

void loop(){
  double dt = float(micros() - Timer) / 1000000.0;
  Timer = micros();
  for(short i=0; i < NUM_MOTORS; i++){
      servomotors[i]->run(dt);
  }
  getImuData();


  sendRobotData();
  nh.spinOnce();

  publishInfoFreq();
  counts_loop += 1;
}
