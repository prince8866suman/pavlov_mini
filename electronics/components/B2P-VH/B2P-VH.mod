PCBNEW-LibModule-V1  2021-10-17 12:37:35
# encoding utf-8
Units mm
$INDEX
SHDR2W114P0X396_1X2_786X850X940P
$EndINDEX
$MODULE SHDR2W114P0X396_1X2_786X850X940P
Po 0 0 0 15 616c0aff 00000000 ~~
Li SHDR2W114P0X396_1X2_786X850X940P
Cd B2P-VH(LF)(SN)
Kw Connector
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "J**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "SHDR2W114P0X396_1X2_786X850X940P"
DS -2.2 4.5 -2.2 -4.5 0.05 24
DS -2.2 -4.5 6.16 -4.5 0.05 24
DS 6.16 -4.5 6.16 4.5 0.05 24
DS 6.16 4.5 -2.2 4.5 0.05 24
DS -1.95 4.25 -1.95 -4.25 0.1 24
DS -1.95 -4.25 5.91 -4.25 0.1 24
DS 5.91 -4.25 5.91 4.25 0.1 24
DS 5.91 4.25 -1.95 4.25 0.1 24
DS 0 4.25 5.91 4.25 0.2 21
DS 5.91 4.25 5.91 -4.25 0.2 21
DS 5.91 -4.25 -1.95 -4.25 0.2 21
DS -1.95 -4.25 -1.95 0 0.2 21
$PAD
Po 0 0
Sh "1" R 2.7 2.7 0 0 900
Dr 1.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 3.96 0
Sh "2" C 2.7 2.7 0 0 900
Dr 1.8 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE SHDR2W114P0X396_1X2_786X850X940P
$EndLIBRARY
