#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from evdev import InputDevice, categorize, ecodes
from geometry_msgs.msg import TwistStamped, PoseStamped
import numpy as np
from tf import transformations
from settings import *
from select import select
from pavlov_mini_msgs.srv import set_gait, set_gaitRequest, set_gaitResponse
from pavlov_mini_msgs.srv import connect_servos, connect_servosRequest, connect_servosResponse
import time

class Axis():
    def __init__(self, min_val, max_val, rest_val, out_min = -1, out_max = 1, threshold = 0):
        self.min_val = min_val
        self.max_val = max_val
        self.rest_val = rest_val
        self.out_min = out_min
        self.out_max = out_max
        self.threshold = threshold
        
    def mapValue(self, val):
        in_min = self.min_val - self.rest_val
        in_max = self.max_val - self.rest_val
        mapped_val = (val - in_min) * float(self.out_max - self.out_min) / (in_max - in_min) + self.out_min;
        
        
        if abs(mapped_val) < self.threshold:
            return 0
        return mapped_val
        
        
    def getValue(self, event):
        val = event.value
        val = val - self.rest_val
        val = self.mapValue(val)
        if np.abs(val) < 0.35:
            val = 0
        return val
    
class Button():
    def __init__(self):
            self.state = 0
        
    def getPressed(self, event):
        if event.value == 1:
            self.state = 1
            return True
        else:
            self.state = 0
            return False
        
        
class DoubleButton():
    def __init__(self, out_min = -1, out_max = 1):
        self.state = 0
        self.out_min = out_min
        self.out_max = out_max
        
    def getPressed(self, event):
        if event.value == 1:
            self.state = self.out_max
        elif event.value == -1:
            self.state = self.out_min
        else:
            self.state = 0
        
    def getState(self):
        return self.state


class FilterSmooth():
    def __init__(self, factor = 0):
        self.factor = factor
        self.input  = 0
        self.dxdt   = 0
        self.x      = 0 

    def set(self, val):
        self.input = val
        
    def update(self):
        self.dxdt = self.factor*(-self.x + self.input)
        self.x = self.x + self.dxdt
        
        
    def get(self):
        return self.x


class ManualControlNode:
    def __init__(self, bluetooth_port = '/dev/input/event0'):

        self.debug("INFO", "xBox controller node is running!")
        connected = False
        
        while not connected:
            try:
                self.controller = InputDevice(bluetooth_port)
                connected = True
            except:
                rospy.logerr('XBox controller not connected')
                time.sleep(2)
                pass
        rospy.logerr('XBox controller connected!')

        self.forwardAxis = Axis(FORWARD_AXIS_MIN_VALUE, FORWARD_AXIS_MAX_VALUE, FORWARD_AXIS_REST_VALUE, MIN_FORWARD_VEL, MAX_FORWARD_VEL)
        self.thAxis      = Axis(TH_AXIS_MIN_VALUE, TH_AXIS_MAX_VALUE, TH_AXIS_REST_VALUE, MIN_TH_VEL, MAX_TH_VEL, threshold = 0.5)
        self.rollAxis      = Axis(ROLL_AXIS_MIN_VALUE, ROLL_AXIS_MAX_VALUE, ROLL_AXIS_REST_VALUE, MIN_ROLL_VEL, MAX_ROLL_VEL, threshold = 0.5)

        self.sideLeftAxis      = Axis(SIDE_LEFT_AXIS_MIN_VALUE, SIDE_LEFT_AXIS_MAX_VALUE, SIDE_LEFT_AXIS_REST_VALUE, MIN_SIDE_LEFT_VEL, MAX_SIDE_LEFT_VEL)
        self.sideRightAxis     = Axis(SIDE_RIGHT_AXIS_MIN_VALUE, SIDE_RIGHT_AXIS_MAX_VALUE, SIDE_RIGHT_AXIS_REST_VALUE, MIN_SIDE_RIGHT_VEL, MAX_SIDE_RIGHT_VEL)
        
        self.zAxis       = DoubleButton(MIN_Z_VEL, MAX_Z_VEL)
        self.pitchAxis   = DoubleButton(PITCH_AXIS_MIN_VALUE, PITCH_AXIS_MAX_VALUE)

            
        self.connectServoButton    = Button()
        self.disconnectServoButton = Button()
        
        
        self.setStop         = Button()
        self.setDiagonal     = Button()
        self.setDiagonalFast = Button()
        self.setTrot         = Button()
        self.setInplace      = Button()

        self.forward_vel    = FilterSmooth(0.2)
        self.th_vel         = FilterSmooth(0.2)
        self.roll_vel       = FilterSmooth(0.2)
        self.pitch_vel      = FilterSmooth(0.2)
        self.side_left_vel  = FilterSmooth(0.2)
        self.side_right_vel = FilterSmooth(0.2)
        self.z_vel          = FilterSmooth(0.2)

        
        ####### publisher ##########
        self.velPub               = rospy.Publisher("/pavlov_mini/vel_cmd", TwistStamped, queue_size=10)
        
        ####### clients ############
        self.connectServosClient  = rospy.ServiceProxy('pavlov_mini/connect_servos', connect_servos)
        self.setGaitClient        = rospy.ServiceProxy('pavlov_mini/set_gait', set_gait)


    def setGait(self, gait_filename):
        req = set_gaitRequest()
        req.gait_filename = gait_filename
        try:
            self.setGaitClient(req)
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed: %s" % e)




    """ Code for the main thread of the node """
    def mainThread(self):
        #select([self.controller], [], [])
        
        try:
            for event in self.controller.read():

                #if event.type == ecodes.EV_KEY:
                #    print(event)
    
                if event.code == CODE_FORWARD_AXIS:
                    self.forward_vel.set(  self.forwardAxis.getValue(event)  )
                    
                if event.code == CODE_TH_AXIS:
                    self.th_vel.set(  self.thAxis.getValue(event)  )

                if event.code == CODE_SIDE_LEFT_AXIS:
                    self.side_left_vel.set(  self.sideLeftAxis.getValue(event)  )
                    
                if event.code == CODE_SIDE_RIGHT_AXIS:
                    self.side_right_vel.set( self.sideRightAxis.getValue(event) )
                    
                if event.code == CODE_Z_AXIS:
                    self.zAxis.getPressed(event)
                    
                if event.code == CODE_ROLL_AXIS:
                    self.roll_vel.set(  self.rollAxis.getValue(event)  )
                    
                if event.code == CODE_PITCH_AXIS:
                    self.pitchAxis.getPressed(event)
                    self.pitch_vel.set( MAX_PITCH_VEL * self.pitchAxis.getState() )
                    
                    
                    
                if event.code == CODE_CONNECT_SERVOS_BUTTON:
                    if self.connectServoButton.getPressed(event):
                        rospy.loginfo("connect servos!")
    
                        req = connect_servosRequest()
                        req.connect = True
                        try:
                            self.connectServosClient(req)
                        except rospy.ServiceException as e:
                            rospy.logwarn("Service call failed: %s" % e)
                        
                if event.code == CODE_DISCONNECT_SERVOS_BUTTON:
                    if self.disconnectServoButton.getPressed(event):
                        rospy.loginfo("disconnect servos!")
                        req = connect_servosRequest()
                        req.connect = False
                        try:
                            self.connectServosClient(req)
                        except rospy.ServiceException as e:
                            rospy.logwarn("Service call failed: %s" % e)
                        
                
                
                if event.code == CODE_STOP_SERVOS_BUTTON:
                    if self.setStop.getPressed(event):
                        self.setGait("stop")
                
                
                
                if event.code == CODE_DIAGONAL_SERVOS_BUTTON:
                    if self.setDiagonal.getPressed(event):
                        self.setGait("diagonal")
                        
                        
                if event.code == CODE_DIAGONAL_FAST_SERVOS_BUTTON:
                    if self.setDiagonalFast.getPressed(event):
                        self.setGait("diagonal_fast")
                        
                        
                if event.code == CODE_TROT_SERVOS_BUTTON:
                    if self.setTrot.getPressed(event):
                        self.setGait("trot_slow")
                        
                if event.code == CODE_INPLACE_SERVOS_BUTTON:
                    if self.setInplace.getPressed(event):
                        self.setGait("inplace")
                        
        except:
             pass
                                        
                
        self.forward_vel.update()
        self.th_vel.update()
        self.side_left_vel.update()
        self.side_right_vel.update()
        self.z_vel.update()
        self.pitch_vel.update()
        self.roll_vel.update()


                
        side_velocity = self.side_left_vel.get() + self.side_right_vel.get()
        #print(self.forward_vel, self.th_vel, side_velocity, self.z_vel)
            
        self.z_vel.set( self.zAxis.getState() )
            
        velCmd = TwistStamped()
        velCmd.header.stamp = rospy.Time.now()
        velCmd.twist.linear.x = self.forward_vel.get()
        velCmd.twist.linear.y = side_velocity
        velCmd.twist.linear.z = self.z_vel.get()
        velCmd.twist.angular.z = self.th_vel.get()
        velCmd.twist.angular.y = self.roll_vel.get()
        velCmd.twist.angular.x = self.pitch_vel.get()

        self.velPub.publish(velCmd)
        
            
    def debug(self, typ, msg):
        print typ + ": " + msg + "\n"


if __name__ == '__main__':
    try:
        rospy.init_node('xbox_controller_node', anonymous=True)
        rate = rospy.Rate(10)    # 10 Hz
        node = ManualControlNode()

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
        
        